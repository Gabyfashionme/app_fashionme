import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal-image',
  templateUrl: 'modal-image.html',
})
export class ModalImagePage {
  
  img: string;
  title: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.img = this.navParams.get('img');
    this.title = this.navParams.get('title');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
