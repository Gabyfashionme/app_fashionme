import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalImagePage } from './modal-image';

@NgModule({
  declarations: [
    ModalImagePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalImagePage),
  ],
  exports: [
    ModalImagePage
  ]
})
export class ModalImagePageModule {}
