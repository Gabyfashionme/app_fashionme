import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';
import { AuthService } from '../providers/auth-service';

import { Camera } from '@ionic-native/camera';
import { SQLite } from '@ionic-native/sqlite';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Register } from '../pages/register/register';
import { Login } from '../pages/login/login';
import { Tabs } from '../pages/tabs/tabs';
import { Welcome } from '../pages/welcome/welcome';
import { Photo } from '../pages/photo/photo';
import { BodyType } from '../pages/body-type/body-type';
import { Especificaciones } from '../pages/especificaciones/especificaciones';
import { Colorimetria } from '../pages/colorimetria/colorimetria';
import { ImagesPage } from '../pages/images/images';
import { ClosetsPage } from '../pages/closets/closets';
import { CategoriasPage } from '../pages/categorias/categorias';
import { SubcategoriasPage } from '../pages/subcategorias/subcategorias';
import { ModalImagePage } from '../pages/modal-image/modal-image';
import { StartPage } from '../pages/start/start';
import { ColorResultsPage } from '../pages/color-results/color-results';
import { ProfilePage } from '../pages/profile/profile';
import { TrendsPage } from '../pages/trends/trends';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Register,
    Login, 
    Tabs, 
    Welcome, 
    Photo,
    BodyType,
    Especificaciones,
    Colorimetria, 
    ImagesPage, 
    ClosetsPage, 
    CategoriasPage, 
    SubcategoriasPage, 
    ModalImagePage, 
    StartPage, 
    ColorResultsPage, 
    ProfilePage,
    TrendsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Register,
    Login, 
    Tabs, 
    Welcome, 
    Photo,
    BodyType,
    Especificaciones,
    Colorimetria, 
    ImagesPage, 
    ClosetsPage, 
    CategoriasPage, 
    SubcategoriasPage, 
    ModalImagePage, 
    StartPage, 
    ColorResultsPage, 
    ProfilePage,
    TrendsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService, 
    Camera, 
    SQLite
  ]
})
export class AppModule {}
