import Dexie from 'dexie';

export class FashionmeAppDB extends Dexie {
	
	users: Dexie.Table<IUser, number>;
	data: Dexie.Table<IData, number>;

	constructor() {
		super('FashionmeAppDB');
		this.version(1).stores({
			// users: '++id, nombre, sexo, email, password',
			data: '++id, data1, data2'
		});
		
		// this.users.mapToClass(User);
		this.data.mapToClass(Data);
	}

}

export class Data implements IData{
	id?: number;
	data1: string;
	data2: string;

	constructor(data1: string, data2:string, id?: number){
		this.data1 = data1;
		this.data2 = data2;
		if(id) this.id = id;
	}

	save(){
		db.data.put(this);
		return db.data.add(this);
	}

	// Retorna un Promise de Data
	static all(){
		return db.data.orderBy('id').toArray();
	}
}

export interface IData{
	id?: number;
	data1: string;
	data2: string;
}

export class User implements IUser{
	nombre: string;
	sexo:string;
	email:string;
	password:string;
	password_confirmation: string;
	id?:number;

	constructor(nombre: string, sexo:string, email:string, password:string, password_confirmation:string, id?:number){
		this.nombre = nombre;
		this.sexo = sexo;
		this.email = email;
		this.password = password;
		this.password_confirmation = password_confirmation;
		if(id) this.id = id;
	}

	save(){
		db.users.put(this);
		return db.users.add(this);
	}

	// Retorna un Promise de todos los usuarios
	static all(){
		return db.users.orderBy('id').toArray();
	}
}

export interface IUser{
	id?:number;
	nombre: string;
	sexo:string;
	email:string;
	password:string;
	password_confirmation: string;
}

export let db = new FashionmeAppDB;